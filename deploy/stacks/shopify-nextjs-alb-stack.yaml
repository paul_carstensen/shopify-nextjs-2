AWSTemplateFormatVersion: 2010-09-09
Description: ALB stack

Parameters:
  EnvironmentName:
    Description: An environment name. Will be used for prefixing
    Type: String
  Subnets:
    Type: List<AWS::EC2::Subnet::Id>
    Description: The list of SubnetIds
  VpcId:
    Type: AWS::EC2::VPC::Id
  AlarmingSNSTopic:
    Description: SNS Topic for alarming
    Type: String
  AppUptodateDeZoneId:
    Type: String
    Description: Zone ID for app.uptodate.de
  UptodateDeZoneId:
    Type: String
    Description: Zone ID for uptodate.de

Conditions:
  IsProd: !Equals [prod, !Ref EnvironmentName]

Mappings:
  ARNs:
    ALBCertificate:
      "dev": "arn:aws:acm:eu-central-1:951250281069:certificate/2f347860-ce52-4e00-bce4-abbce2044994"
      "prod": "arn:aws:acm:eu-central-1:951250281069:certificate/490daabf-ccf6-432b-ad32-8493d0a29a2b"

Resources:
  ApplicationLoadBalancer:
    Type: AWS::ElasticLoadBalancingV2::LoadBalancer
    Properties:
      Subnets: !Ref Subnets
      Name: !Sub ${AWS::StackName}
      Scheme: internet-facing
      SecurityGroups:
        - !Ref ALBSecurityGroup

  ALBSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupName: !Sub "shopify-nextjs-sg-${EnvironmentName}"
      GroupDescription: Enable HTTP and HTTPS access on the inbound port on ALB
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: '443'
          ToPort: '443'
          CidrIp: 0.0.0.0/0
        - IpProtocol: tcp
          FromPort: '443'
          ToPort: '443'
          CidrIpv6: ::/0
      VpcId: !Ref VpcId

  ALBListenerForHttps:
    Type: AWS::ElasticLoadBalancingV2::Listener
    Properties:
      Certificates:
        - CertificateArn: !FindInMap [ARNs, ALBCertificate, !Ref EnvironmentName]
      DefaultActions:
        - Type: fixed-response
          FixedResponseConfig:
            ContentType: text/plain
            MessageBody: "Service Unavailable"
            StatusCode: 503
      LoadBalancerArn: !Ref ApplicationLoadBalancer
      Port: 443
      Protocol: HTTPS

  Route53CNAMERecord:
    Type: AWS::Route53::RecordSet
    Properties:
      HostedZoneId: !If [IsProd, !Ref UptodateDeZoneId, !Ref AppUptodateDeZoneId]
      Type: CNAME
      Name: !If [IsProd, "nextjs.uptodate.de.", !Sub "nextjs-${EnvironmentName}.app.uptodate.de."]
      ResourceRecords:
        - !GetAtt ApplicationLoadBalancer.DNSName
      TTL: 300

  5xxAlarm:
    Type: AWS::CloudWatch::Alarm
    Properties:
      AlarmActions:
        - !Ref AlarmingSNSTopic
      AlarmDescription: !Sub Alarm if 5xx in ${AWS::StackName} ALB happens
      AlarmName: !Sub ${AWS::StackName}-5xxAlarm
      Dimensions:
        - Name: LoadBalancer
          Value: !Ref ApplicationLoadBalancer
      Namespace: AWS/ApplicationELB
      MetricName: HTTPCode_Target_5XX_Count
      ComparisonOperator: GreaterThanThreshold
      Statistic: Sum
      Threshold: 0
      Period: 60
      EvaluationPeriods: 5

  LatencyAlarm:
    Type: AWS::CloudWatch::Alarm
    Properties:
      AlarmActions:
        - !Ref AlarmingSNSTopic
      AlarmDescription: !Sub Alarm if latency in ${AWS::StackName} is too long
      AlarmName: !Sub ${AWS::StackName}-LatencyAlarm
      Dimensions:
        - Name: LoadBalancer
          Value: !Ref ApplicationLoadBalancer
      Namespace: AWS/ApplicationELB
      MetricName: TargetResponseTime
      ComparisonOperator: GreaterThanThreshold
      ExtendedStatistic: p99
      Threshold: 700
      Period: 60
      EvaluationPeriods: 5

Outputs:
  ALBEndpoint:
    Description: Endpoint of the ALB
    Value: !If [IsProd, "nextjs.uptodate.de", !Sub "nextjs-${EnvironmentName}.app.uptodate.de"]
  ListenerARN:
    Description: ARN of the ALB Listener
    Value: !Ref ALBListenerForHttps
  ALBSecurityGroup:
    Description: Security group from ALB
    Value: !Ref ALBSecurityGroup
