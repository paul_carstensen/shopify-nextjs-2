#!/bin/bash

set -Eeuo pipefail

source ./bash_utils/stack.sh
source ./bash_utils/params.sh

AWS_REGION=eu-central-1

check_environment_var

STACK="shopify-nextjs-cloudfront-$ENVIRONMENT"

alb_output=$(stack_output shopify-nextjs-alb-$ENVIRONMENT)
alb_domain=$(param_from_stack_output "$alb_output" ALBEndpoint)

app_zone_output=$(stack_output shopify-nextjs-route53)
app_zone_id=$(param_from_stack_output "$app_zone_output" ZoneId)

aws cloudformation deploy \
  --region $AWS_REGION \
  --stack-name $STACK \
  --template-file stacks/shopify-nextjs-cloudfront-stack.yaml \
  --no-fail-on-empty-changeset \
  --parameter-overrides EnvironmentName=$ENVIRONMENT \
                        ALBDomainName=$alb_domain \
                        AppUptodateDeZoneId=$app_zone_id
