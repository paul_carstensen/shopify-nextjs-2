#!/bin/bash

set -Eeuo pipefail

source ./bash_utils/params.sh

AWS_REGION=eu-central-1

check_environment_var

STACK="shopify-nextjs-logs-$ENVIRONMENT"

aws cloudformation deploy \
 --region $AWS_REGION \
 --stack-name $STACK \
 --template-file stacks/shopify-nextjs-logs-stack.yaml \
 --no-fail-on-empty-changeset \
 --parameter-overrides EnvironmentName=$ENVIRONMENT
