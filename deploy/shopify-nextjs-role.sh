#!/bin/bash

set -Eeuo pipefail

source ./bash_utils/stack.sh
source ./bash_utils/params.sh

AWS_REGION=eu-central-1

check_environment_var

STACK="shopify-nextjs-role-$ENVIRONMENT"

cognito_output=$(stack_output graphql-cognito-$ENVIRONMENT)
cognito_arn=$(param_from_stack_output "$cognito_output" UserPoolArn)

aws cloudformation deploy \
  --region $AWS_REGION \
  --stack-name $STACK \
  --template-file stacks/shopify-nextjs-role-stack.yaml \
  --capabilities CAPABILITY_NAMED_IAM \
  --no-fail-on-empty-changeset \
  --parameter-overrides ArtifactsBucket=uptodate-artifacts-$AWS_REGION \
                        PublicKeyBucket=uptodate-public-keys-$AWS_REGION \
                        CognitoARN=$cognito_arn
