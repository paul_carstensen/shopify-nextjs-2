#!/bin/bash

set -Eeuo pipefail

if [ "${1-}" != "--use-existing" ]; then
   echo "Fetching CodeArtifact token…"
  ./ca-token.sh
fi

echo "Configuring npm to use CodeArtifact…"
npm config set registry https://uptodate-951250281069.d.codeartifact.eu-central-1.amazonaws.com/npm/marketplace/
npm config set //uptodate-951250281069.d.codeartifact.eu-central-1.amazonaws.com/npm/marketplace/:_authToken "$(cat .codeartifact-token)"
npm config set //uptodate-951250281069.d.codeartifact.eu-central-1.amazonaws.com/npm/marketplace/:always-auth true
