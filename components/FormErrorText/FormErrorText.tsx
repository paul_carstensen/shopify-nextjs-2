import styled from 'styled-components';

export const FormErrorText = styled.span`
  display: inline-block;
  text-align: center;
  color: #e44b4b;
  font-size: 11px;
`;
