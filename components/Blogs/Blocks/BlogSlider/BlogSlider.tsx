import React from 'react';
import styled from 'styled-components';
import { Slider } from '@u2dv/marketplace_ui_kit/dist/components';
import { BlogPostItem, BlogPostItemFromGqlPrefetch, BlogPostItemProps } from '../BlogPostItem';
import { PageWidthContainer } from '../../../common/PageWidthContainer';
import { HeaderAnchorOffset } from '../../../common/HeaderAnchorOffset';
import { cleanElementId } from '../../../../helpers/cleanElementId';

const BlogSliderContainer = styled.div`
  width: 80%;
  margin: auto;
  @media screen and (max-width: 768px) {
    margin-top: 20px;
    width: 100%;
  }
`;

const SliderTitle = styled.div`
  font-size: 24px;
  text-transform: uppercase;
  font-weight: 500;
  line-height: 1.5;
  text-align: center;
  margin-bottom: 20px;
  @media screen and (max-width: 768px) {
    margin-bottom: 14px;
  }
`;

export type BlogSliderProps = {
  posts: BlogPostItemFromGqlPrefetch[];
  blogAndPostSlugs: string[];
  sliderTitle?: string;
}

const mapPropsToBlogPostProps = (posts: BlogPostItemFromGqlPrefetch[], blogAndPostSlugs: string[]): BlogPostItemProps[] => posts.map((post, i) => {
  const [blogSlug, slug] = blogAndPostSlugs[i].split('/', 2);

  return { ...post, slug, blog: { slug: blogSlug } };
});

export const BlogSlider: React.FC<BlogSliderProps> = ({ posts, blogAndPostSlugs, sliderTitle }) => {
  const blogPostProps = mapPropsToBlogPostProps(posts, blogAndPostSlugs);
  const blockId = sliderTitle ? `blog_slider_${sliderTitle}` : 'blog_slider';
  return (
    <HeaderAnchorOffset id={cleanElementId(blockId)}>
      <PageWidthContainer>
        <BlogSliderContainer>
          {sliderTitle && <SliderTitle>{sliderTitle}</SliderTitle> }
          <Slider
            visibleSlidesBreakpoints={[
              { breakpoint: '(max-width: 560px)', value: 1 },
              { breakpoint: '(min-width: 561px)', value: 2 },
            ]}
            enableArrows
            enableMouseWheel
            trackingId="ga-blog-blog-slider-click"
          >
            {blogPostProps.map(({ title, readingTimeMinutes, publishedAt, blogPostId, imageUrl, slug }) => (
              <BlogPostItem
                title={title}
                readingTimeMinutes={readingTimeMinutes}
                publishedAt={publishedAt}
                blogPostId={blogPostId}
                imageUrl={imageUrl}
                slug={slug}
                key={blogPostId}
              />
            ))}
          </Slider>
        </BlogSliderContainer>
      </PageWidthContainer>
    </HeaderAnchorOffset>
  );
};
