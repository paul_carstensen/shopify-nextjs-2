import styled from 'styled-components';

export const Heading2 = styled.h2`
  margin: 0 0 7.5px;
  font-family: "Quicksand", sans-serif;
  font-weight: 400;
  letter-spacing: 0em;
  line-height: 1;
  text-transform: uppercase;
  font-size: 1.72em;
  @media only screen and (min-width: 769px) {
    margin: 0 0 15px;
  }
`;
