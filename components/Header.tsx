import React from 'react';
import Link from 'next/link';
import Image from 'next/image';
import { NavigationItem } from '../types';
import { HeaderDrawerMenu } from './HeaderDrawerMenu';
import { HeaderMenu } from './HeaderMenu';
import { ShopifyGlobalStyle } from './ShopifyGlobalStyle';
import uptodateLogoSlogan from '../assets/images/uptodate_logo_website-only.svg';

type HeaderProps = {
  mainNavigationItems: NavigationItem[];
  sideNavigationItems: NavigationItem[];
}

const Header: React.FC<HeaderProps> = ({ mainNavigationItems, sideNavigationItems }) => {
  const halfNavigation = (mainNavigationItems.length - 1) / 2;
  const mainNavigationsLeft = mainNavigationItems.filter((_, i) => i <= halfNavigation);
  const mainNavigationsRight = mainNavigationItems.filter((_, i) => i > halfNavigation);

  return (
    <div id="shopify-section-header" className="shopify-section">
      <ShopifyGlobalStyle />
      <div id="NavDrawer" className="drawer drawer--left">
        <div className="drawer__fixed-header drawer__fixed-header--full">
          <div className="drawer__header drawer__header--full appear-animation appear-delay-1">
            <div className="h2 drawer__title" />
            <div className="drawer__close">
              <button
                id="hamburger-close"
                type="button"
                className="drawer__close-button js-drawer-close"
              >
                <svg
                  aria-hidden="true"
                  focusable="false"
                  role="presentation"
                  className="icon icon-close pointer-none"
                  viewBox="0 0 64 64"
                >
                  <path d="M19 17.61l27.12 27.13m0-27.12L19 44.74" />
                </svg>
                <span className="icon__fallback-text">Menü schließen</span>
              </button>
            </div>
          </div>
        </div>
        <HeaderDrawerMenu mainNavigationItems={mainNavigationItems} sideNavigationItems={sideNavigationItems} />
      </div>

      <div data-section-id="header" data-section-type="header-section">

        <div className="header-sticky-wrapper">
          <div className="header-wrapper">
            <header
              className="site-header site-header--heading-style"
              data-sticky="true"
            >
              <div className="page-width header-section">
                <div
                  className="header-layout header-layout--center-split"
                  data-logo-align="center"
                >
                  <div className="header-item header-item--left header-item--navigation">
                    <div className="site-nav">
                      <button
                        id="hamburger-btn"
                        type="button"
                        className="site-nav__link site-nav__link--icon js-drawer-open-nav"
                        aria-controls="NavDrawer"
                      >
                        <svg
                          aria-hidden="true"
                          focusable="false"
                          role="presentation"
                          className="icon icon-hamburger pointer-none"
                          viewBox="0 0 64 64"
                        >
                          <path d="M7 15h51M7 32h43M7 49h51" />
                        </svg>
                        <span className="icon__fallback-text">
                          Seitennavigation
                        </span>
                      </button>
                    </div>
                  </div>

                  <div
                    className="header-item header-item--logo-split"
                    role="navigation"
                    aria-label="Primary"
                  >
                    <div className="header-item header-item--split-left">
                      <HeaderMenu links={mainNavigationsLeft} />
                    </div>
                    <div className="header-item header-item--logo">
                      <Link href="/">
                        <a itemProp="url" className="site-header__logo-link">
                          <Image
                            src={uptodateLogoSlogan}
                            alt="uptodate – Dein Leben. Deine Zukunft."
                            width={128}
                            height={60}
                          />
                        </a>
                      </Link>
                    </div>
                    <div className="header-item header-item--split-right">
                      <HeaderMenu links={mainNavigationsRight} />
                    </div>
                  </div>

                  <div className="header-item header-item--icons" />
                </div>
              </div>
            </header>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
