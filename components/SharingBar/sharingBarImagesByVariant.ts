import emailImage from '../../assets/images/sharing/email.png';
import emailWhiteImage from '../../assets/images/sharing/email-white.png';
import whatsAppImage from '../../assets/images/sharing/whatsapp.png';
import whatsAppWhiteImage from '../../assets/images/sharing/whatsapp-white.png';
import facebookImage from '../../assets/images/sharing/facebook.png';
import facebookWhiteImage from '../../assets/images/sharing/facebook-white.png';
import twitterImage from '../../assets/images/sharing/twitter.png';
import twitterWhiteImage from '../../assets/images/sharing/twitter-white.png';

export type StaticImageData = typeof emailImage;

type VariantImagesData = {
  emailImage: StaticImageData;
  whatsAppImage: StaticImageData;
  facebookImage: StaticImageData;
  twitterImage: StaticImageData;
}

export const sharingBarImageByVariant = (variant?: string): VariantImagesData => {
  if (variant === 'white') {
    return {
      emailImage: emailWhiteImage,
      whatsAppImage: whatsAppWhiteImage,
      facebookImage: facebookWhiteImage,
      twitterImage: twitterWhiteImage,
    };
  }

  return {
    emailImage,
    whatsAppImage,
    facebookImage,
    twitterImage,
  };
};
