import React, { useState, useEffect } from 'react';
import { Button } from '@u2dv/marketplace_ui_kit/dist/components';
import styled from 'styled-components';
import { SliderContainer } from '../../../SliderContainer';
import watermark from '../../../../assets/images/sustainability/watermark-slide.png';

export const SliderEarnest: React.FC = () => {
  const [shouldRenderButton, setShouldRenderButton] = useState<boolean>(false);

  useEffect(() => {
    setShouldRenderButton(true);
  }, [shouldRenderButton]);

  const StyledButton = styled.div`
    margin-right: 20px;
    & a:hover {
      color: white;
    }
`;

  return (
    <SliderContainer
      counter="5"
      numSlides="5"
      backgroundColor="#ede3bb"
      backgroundImage={watermark}
      watermarkBg
      title="Klasse, jetzt bist Du einen Schritt näher am nachhaltigen Leben!"
      subtitle="Du willst noch mehr?"
      text="Dann lade Dir jetzt unsere Earnest App herunter und entdecke Deinen Begleiter für ein nachhaltigeres Leben."
    >
      {shouldRenderButton && (
        <StyledButton>
          <Button
            as="a"
            href="https://app.adjust.com/zb8irhh?fallback=https%3A%2F%2Fwww.meetearnest.de%2F"
            target="_blank"
            rel="noopener noreferrer"
            data-testid="btn-earnest-continue"
            trackingId="ga-sust-for-beginners-earnest-continue"
          > Los geht&apos;s
          </Button>
        </StyledButton>
      )}
    </SliderContainer>
  );
};
