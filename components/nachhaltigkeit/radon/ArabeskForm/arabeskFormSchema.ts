import * as Yup from 'yup';

export default Yup.object().shape({
  arabeskWaterFilter: Yup.boolean()
    .required('Eine Auswahl wird benötigt'),
  arabeskWaterFilterMaintenance: Yup.boolean()
    .when('arabeskWaterFilter', {
      is: true,
      then: Yup.boolean().required('Eine Auswahl wird benötigt'),
      otherwise: Yup.boolean(),
    }),
  arabeskWaterRarelyUsedSources: Yup.boolean()
    .required('Eine Auswahl wird benötigt'),
  arabeskWaterSiliconeGroutOk: Yup.boolean()
    .required('Eine Auswahl wird benötigt'),
  arabeskSmokeAlarms: Yup.boolean()
    .required('Eine Auswahl wird benötigt'),
  arabeskCircuitBreakers: Yup.boolean()
    .required('Eine Auswahl wird benötigt'),
  arabeskPropertyOwner: Yup.boolean()
    .required('Eine Auswahl wird benötigt'),
  arabeskMovingOutSoon: Yup.boolean()
    .required('Eine Auswahl wird benötigt'),
});
