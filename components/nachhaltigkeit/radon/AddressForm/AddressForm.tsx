/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import { useRef, useEffect, useState } from 'react';
import { useFormik } from 'formik';
import styled from 'styled-components';
import addressFormSchema from './addressFormSchema';
import { Input } from '../../../Input';
import { FormErrorText } from '../../../FormErrorText';
import { SanitizedHTML } from '../../../SanitizedHTML';
import { RadonPolicyModal } from '../RadonPolicyModal';
import { useApiErrors } from '../../../../context/errors/apiErrorsProvider';

const InputsContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  column-gap: 30px;
  grid-template-areas:
    "streetName streetName"
    "houseNumber postalCode"
    "email email"
    "consent consent";
  & [for="streetName"] {
    grid-area: streetName;
  }
  & [for="postalCode"] {
    grid-area: postalCode;
  }
  & [for="houseNumber"] {
    grid-area: houseNumber;
  }
  & [for="email"] {
    grid-area: email;
  }
`;

const ConsentComponent = styled.div`
  grid-area: consent;
  & label div {
    display: inline-block;
    width: 200px;
    vertical-align: text-bottom;
    letter-spacing: 0em;
    text-transform: none;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }
  & label div :first-child {
    display: inline;
  }
  & label div :not(:first-child) {
    display: none;
  }
  & label {
    margin-bottom: 30px;
  }
  & button {
    margin: -36px 0 10px 16px;
    position: absolute;
    font-weight: bold;
  }
`;

const FormContainer = styled.form`
  backdrop-filter: blur(10px);
  background-color: rgba(255, 255, 255, 0.75);
  z-index: 1;
  grid-area: form;
  @media screen and (min-width: 769px) {
    padding: 35px 48px 45px 48px;
    width: 264px;
    height: calc(400px - 80px);
    margin-left: -392px;
    margin-top: 16px;
  }
  @media screen and (min-width: 1480px) {
    margin-left: calc(-50vw + 710px - 340px);
  }
  @media screen and (max-width: 768px) {
    width: calc(100% - 48px - 64px);
    margin: 0 24px;
    padding: 0 32px 20px 32px;
    font-family: Quicksand;
    margin-top: -418px;
    height: max-content;
    & label {
      font-size: 12px;
      font-weight: 500;
      font-stretch: normal;
      font-style: normal;
      line-height: normal;
      letter-spacing: 3px;
    }
  }
`;

const FullLineInput = styled(Input)`
  display: flex;
  flex-direction: column;
  width: 100%;
  &:last-child {
    margin: 10px 0 0 0; /* Margin bottom is set by theme.scss :< */
    width: calc(100% - 22px);
    background-color: white;
  }
`;

const SubmitButtonContainer = styled.div`
`;

const SubmitButton = styled.button.attrs({
  type: 'submit',
  'data-tracking-id': 'ga-radon-address-submit',
})`
  background-color: #19232d;
  color: white;
  border: 1px solid #19232d;
  font-family: Quicksand;
  font-size: 12px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: 3px;
  width: calc(100% - 2px);
  padding: 10px 0;
  text-transform: uppercase;
  text-decoration: none;
  text-align: center;
  border-radius: 0px;
`;

const Label = styled.label`
  text-transform: uppercase;
  letter-spacing: 0.3em;
  font-size: 0.75em;
  display: block;
  margin-bottom: 10px;

  &[for] {
    cursor: pointer;
  }

  &.error {
    color: #d02e2e;
  }
`;

const getFirstFocusableField = (form: HTMLFormElement): HTMLInputElement | undefined => {
  const elements = Array.from(form.elements) as HTMLInputElement[];

  return elements.find((el) => typeof el.focus === 'function');
};

export type AddressForm = {
  policyTextHtml: string;
  isFocused: boolean;
  onSubmit: (data: any) => void
  clearFocus: () => void;
}

export const AddressForm = ({
  onSubmit,
  isFocused,
  clearFocus,
  policyTextHtml,
} : AddressForm) => {
  const formRef = useRef<HTMLFormElement>(null);
  const [policyModalOpen, setPolicyModalOpen] = useState(false);
  const { errors: apiErrors } = useApiErrors();
  const { values, errors, handleSubmit, handleChange, setFieldValue } = useFormik({
    initialValues: {
      email: '',
      houseNumber: '',
      streetName: '',
      postalCode: '',
      consentCheckbox: undefined,
    },
    validationSchema: addressFormSchema,
    validateOnChange: false,
    onSubmit,
  });

  useEffect(() => {
    if (isFocused && formRef.current) {
      const firstInput = getFirstFocusableField(formRef.current);

      firstInput?.focus();
    }
  }, [isFocused]);

  return (
    <FormContainer id="radon-address-form" ref={formRef} onSubmit={handleSubmit}>
      <RadonPolicyModal
        isOpen={policyModalOpen}
        closeModal={() => setPolicyModalOpen(false)}
        policyTextHtml={policyTextHtml}
      />
      <InputsContainer>
        <FullLineInput
          id="streetName"
          label="Straßenname"
          value={values.streetName}
          onChange={handleChange}
          onBlur={clearFocus}
          data-testid="radon-address-form-first-field"
          required
        />
        <FullLineInput
          id="houseNumber"
          label="Hausnummer"
          value={values.houseNumber}
          onChange={handleChange}
          required
        />
        <FullLineInput
          id="postalCode"
          label="PLZ"
          value={values.postalCode}
          onChange={handleChange}
          required
        />
        <FullLineInput
          id="email"
          label="E-Mail-Adresse"
          value={values.email}
          onChange={handleChange}
          required
        />
        <ConsentComponent>
          <Label htmlFor="consent">
            <input
              id="consent"
              type="checkbox"
              name="consent"
              onChange={(e) => setFieldValue('consentCheckbox', e.target.checked)}
            />
            <SanitizedHTML innerHTML={policyTextHtml} />
          </Label>
          <button type="button" onClick={() => setPolicyModalOpen(true)}>Mehr anzeigen</button>
        </ConsentComponent>
      </InputsContainer>

      <SubmitButtonContainer>
        <SubmitButton>
          Weiter
        </SubmitButton>
      </SubmitButtonContainer>

      {apiErrors.radon && <FormErrorText>{apiErrors.radon}</FormErrorText>}
      {Object.values(errors).length ? <FormErrorText>{Object.values(errors)[0]}</FormErrorText> : null}
    </FormContainer>
  );
};
