import React from 'react';
import { SplitView } from '../../../SplitView';
import radonSplitViewImage from '../../../../assets/images/radon/radon-split-view.jpg';

export type RadonSplitViewProps = {
  onCtaClick: () => void;
};

export const RadonSplitView: React.FC<RadonSplitViewProps> = ({ onCtaClick }) => (
  <SplitView
    title="Warum ist Radon potenziell gefährlich?"
    imageUrl={radonSplitViewImage}
    imagePosition="right"
    ctaText="Radon-Analyse starten"
    ctaUrl="#radon-address-form"
    onCtaButtonClick={onCtaClick}
    dataTrackingId="ga-radon-goto-address-form"
  >
    <div>
      <p>Radon ist die häufigste Ursache für Lungenkrebs bei Nichtrauchern.</p>
      <ul>
        <li>Radon ist geruchlos und daher nicht erkennbar.</li>
        <li>Manche Gebiete in Deutschland haben ein hohes Radonpotenzial.</li>
      </ul>
      <p>Informiere Dich jetzt über das Radonpotenzial in Deiner unmittelbaren Umgebung und treffe Vorkehrungen, um gesund zu bleiben.</p>
    </div>
  </SplitView>
);
