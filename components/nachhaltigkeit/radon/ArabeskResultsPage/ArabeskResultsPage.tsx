import React from 'react';
import styled from 'styled-components';
import Hero from '../../../Hero';
import { RadonSharingBar } from '../RadonSharingBar';
import { Text } from '../../../Text';
import { RadonEarnestSplitView } from '../RadonEarnestSplitView';
import hero360x from '../../../../assets/images/radon/gimmick-hero_360x.jpg';
import hero540x from '../../../../assets/images/radon/gimmick-hero_540x.jpg';
import hero1420x from '../../../../assets/images/radon/gimmick-hero_1420x.jpg';
import hero2840x from '../../../../assets/images/radon/gimmick-hero_2840x.jpg';
import { MainContainerWithElementStyles } from '../../../common/MainContainer';

const SharingBarContainer = styled.div`
  text-align: center;
`;

const results = {
  arabeskWaterFilter: {
    false: (
      <p>
        Du hast angegeben, dass hinter der Wasseruhr kein Feinfilter installiert ist. Die DIN 1988 sieht vor, dass bei
        neuen Gebäuden zu Beginn der Hausinstallation ein Filter eingebaut sein muss, der Schwebstoffe und Partikel aus
        dem Wasser filtert. Beauftrage einfach Deinen Installateur mit dem Einbau eines Feinfilters, um Deine
        Trinkwasserinstallation zu schützen!
      </p>
    ),
    true: '',
  },
  arabeskWaterFilterMaintenance: {
    false: (
      <p>
        Du hast angegeben, dass der Feinfilter nicht regelmäßig gereinigt wird. Dein Feinfilter schützt Dich vor
        Verschmutzung Deines Trinkwassers und Deine Rohre vor Korrosionsschäden. Damit er einwandfrei funktioniert muss
        er nach Herstellerangaben gereinigt, rückgespült bzw. die Filterkerze gewechselt werden. Sinnvoll ist es, dies
        mindestens zweimal jährlich durchzuführen. Frag im Zweifelsfall Deinen Installateur, was zu tun ist.
      </p>
    ),
    true: (
      <p>
        Du hast angegeben, dass der Feinfilter regelmäßig gereinigt wird. Dein Feinfilter schützt Dich vor Verschmutzung
        ihres Trinkwassers und Deine Rohre vor Korrosionsschäden. Damit er einwandfrei funktioniert muss er nach
        Herstellerangaben gereinigt, rückgespült bzw. die Filterkerze gewechselt werden.
      </p>
    ),
  },
  arabeskWaterRarelyUsedSources: {
    false: (
      <p>
        Du hast angegeben, dass es keine nicht oder selten genutzte Teile der Wasserversorgung gibt. Achte darauf, dass
        es in Deinem Gebäude keine Bereiche gibt, in denen das Trinkwasser längere Zeit (länger als eine Woche)
        stillsteht. Das kann zu Verkeimung und damit zu gesundheitlichen Risiken führen.
      </p>
    ),
    true: (
      <p>
        Du hast angegeben, dass es nicht oder selten genutzte Teile der Wasserversorgung gibt. Selten oder nicht
        genutzte Teile der Trinkwasserinstallation können zu erheblichen Problemen führen - einerseits zu Korrosion
        andererseits auch zu Verkeimung des Trinkwassers. Daher sollten nicht genutzte Bereiche möglichst zurückgebaut
        werden, wenig genutzte Bereiche hingegen sollten regelmäßig komplett gespült werden
        (mindestens einmal pro Woche).
      </p>
    ),
  },
  arabeskWaterSiliconeGroutOk: {
    false: (
      <p>
        Du hast angegeben, dass in den Nassbereichen die Silikonfugen defekt oder teilweise defekt sind. Durch defekte
        Silikonfugen dringt Wasser in den Wand- und Bodenbereich der Nassräume. Dies führt zu teils heftigen
        Feuchteschäden (z.B. Schimmel, Hausschwamm etc.). Kontrolliere diese daher regelmäßig und ersetze die defekten
        Fugen rechtzeitig - die Lebensdauer liegt je nach Beanspruchung zwischen 5 und 10 Jahren. Ersetze daher die
        defekten Fugen.
      </p>
    ),
    true: (
      <p>
        Du hast angegeben, dass in den Nassbereichen die Silikonfugen in Ordnung sind. Durch defekte Silikonfugen dringt
        Wasser in den Wand- und Bodenbereich der Nassräume. Dies führt zu teils heftigen Feuchteschäden (z.B. Schimmel,
        Hausschwamm etc.). Kontrolliere diese daher regelmäßig und ersetze die defekte Fugen rechtzeitig - die
        Lebensdauer liegt je nach Beanspruchung zwischen 5 und 10 Jahren.
      </p>
    ),
  },
  arabeskSmokeAlarms: {
    false: (
      <>
        <p>
          Du hast angegeben, dass nicht in jedem Raum ein Rauchmelder installiert ist. Beachte unbedingt die
          Mindestausstattung von privaten Wohnräumen mit Rauchmeldern:
        </p>

        <ul>
          <li>
            Schlaf- und Kinderzimmer: Hier sind Rauchmelder gesetzlich vorgeschrieben.
          </li>
          <li>
            Flur: Auch hier besteht die Pflicht zur Installation eines Rauchmelders. Bei offenen Verbindungen innerhalb
            der Wohnung, z. B. Treppen über mehrere Geschosse, sollte mindestens auf der obersten Ebene ein Rauchmelder
            installiert sein.
          </li>
          <li>
            Unser Tipp: Geh auch in Wohn- und Essräumen, im Keller und auf dem Dachboden auf Nummer sicher und
            installiere dort Rauchmelder – für ein rundum sicheres Gefühl.
          </li>
        </ul>
      </>
    ),
    true: (
      <p>
        Du hast angegeben, dass in jedem Raum ein Rauchmelder installiert ist. Unser Tipp: Geh auch in Wohn- und
        Essräumen, im Keller und auf dem Dachboden auf Nummer sicher und installiere dort Rauchmelder – für ein rundum
        sicheres Gefühl.
      </p>
    ),
  },
  arabeskCircuitBreakers: {
    false: (
      <p>
        Du hast angegeben, dass keine Fehlerstromschutzschalter im Sicherungskasten vorhanden sind.
        Fehlerstromschutzschalter tragen erheblich zur Reduzierung lebensgefährlicher Stromunfälle in
        Niederspannungsnetzen bei. Informiere möglichst umgehend Deinen Elektrofachmann, er baut Dir diesen Schalter
        ein.
      </p>
    ),
    true: (
      <p>
        Du hast angegeben, dass Fehlerstromschutzschalter im Sicherungskasten vorhanden sind. Sehr gut, Du besitzt einen
        FI-Schalter (RCD). Er schützt Menschen vor zu lang anhaltenden Stromstößen und dient außerdem als
        Brandschutzvorrichtung.

        Der FI-Schalter sollte zweimal im Jahr getestet werden. Die Fehlerstromschutzschalter verfügen über einen
        kleinen, oft bunten Testknopf. Beim Drücken des Testknopfes wird ein Fehlerstrom simuliert und der Strom sofort
        unterbrochen. Für den Fall, dass sich Dein FI-Schalter beim Testen nicht auslöst, lass unbedingt einen Profi ans
        Werk. Er wird Deinen Schutzschalter überprüfen und bei Bedarf auswechseln.
      </p>
    ),
  },
};

const getArabeskWaterFilterResultText = (arabeskWaterFilter: boolean, arabeskWaterFilterMaintenance: boolean) => {
  if (arabeskWaterFilter) {
    return arabeskWaterFilterMaintenance
      ? results.arabeskWaterFilterMaintenance.true
      : results.arabeskWaterFilterMaintenance.false;
  }
  return results.arabeskWaterFilter.false;
};

export type ArabeskResultsPageProps = {
  arabeskWaterFilter: boolean;
  arabeskWaterFilterMaintenance: boolean;
  arabeskWaterRarelyUsedSources: boolean;
  arabeskWaterSiliconeGroutOk: boolean;
  arabeskSmokeAlarms: boolean;
  arabeskCircuitBreakers: boolean;
};

export const ArabeskResultsPage: React.FC<ArabeskResultsPageProps> = ({
  arabeskWaterFilter,
  arabeskWaterFilterMaintenance,
  arabeskWaterRarelyUsedSources,
  arabeskWaterSiliconeGroutOk,
  arabeskSmokeAlarms,
  arabeskCircuitBreakers,
}) => (
  <MainContainerWithElementStyles>
    <Hero
      title="Empfehlungen"
      subtitle="Auf der Grundlage Deiner Antworten haben wir folgende Empfehlungen für Dich!"
      backgroundImages={[hero360x.src, hero540x.src, hero1420x.src, hero2840x.src]}
      resolutions={[360, 540, 1420, 2840]}
    />
    <SharingBarContainer>
      <RadonSharingBar />
    </SharingBarContainer>
    <Text textAlign="left">
      <span id="lead_tracking">
        {
          getArabeskWaterFilterResultText(arabeskWaterFilter, arabeskWaterFilterMaintenance)
        }
      </span>
    </Text>
    <Text textAlign="left">
      {
        arabeskWaterRarelyUsedSources
          ? results.arabeskWaterRarelyUsedSources.true
          : results.arabeskWaterRarelyUsedSources.false
      }
    </Text>
    <Text textAlign="left">
      {
        arabeskWaterSiliconeGroutOk
          ? results.arabeskWaterSiliconeGroutOk.true
          : results.arabeskWaterSiliconeGroutOk.false
      }
    </Text>
    <Text textAlign="left">
      {
        arabeskSmokeAlarms
          ? results.arabeskSmokeAlarms.true
          : results.arabeskSmokeAlarms.false
      }
    </Text>
    <Text textAlign="left">
      {
        arabeskCircuitBreakers
          ? results.arabeskCircuitBreakers.true
          : results.arabeskCircuitBreakers.false
      }
    </Text>
    <RadonEarnestSplitView />
  </MainContainerWithElementStyles>
);
