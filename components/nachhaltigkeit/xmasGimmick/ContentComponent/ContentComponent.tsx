import React from 'react';
import styled from 'styled-components';

export interface ContentComponentProps {
  title?: string;
  subTitle?: string;
}

const Content = styled.div`
  width: 70%;
  margin: auto;
  padding: 22px 0px;
  @media screen and (max-width: 768px) {
    width: 100%;
    padding: 29px 0px;
  }
`;

const Title = styled.div`
  font-family: Poppins;
  font-size: 28px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.43;
  letter-spacing: normal;
  font-weight: 600;
  text-align: center;
  padding-bottom: 15px;
  @media screen and (max-width: 768px) {
    font-size: 16px;
  }
`;

const SubTitle = styled.div`
  font-family: Poppins;
  font-size: 22px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.36;
  letter-spacing: normal;
  text-align: center;
  @media screen and (max-width: 768px) {
    font-size: 16px;
  }
`;

export const ContentComponent: React.FC<ContentComponentProps> = ({ title, subTitle }) => (
  <Content>
    {title
    && (
      <Title>{title}</Title>
    )}
    {subTitle
    && (
      <SubTitle>{subTitle}</SubTitle>
    )}
  </Content>
);
