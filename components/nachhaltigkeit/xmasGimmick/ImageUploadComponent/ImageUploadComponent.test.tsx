/** @jest-environment jsdom */
import { ReactElement } from 'react';
import { mocked } from 'ts-jest/utils';
import { act, fireEvent, render, waitFor, screen } from '@testing-library/react';
import { ImageUploadComponent } from './ImageUploadComponent';
import readFileAsDataURL from '../../../../helpers/readFileAsDataURL';

jest.mock('../../../../helpers/readFileAsDataURL');

const flushPromises = async (rerender: (ui: ReactElement) => void, ui: ReactElement) => {
  await act(() => waitFor(() => rerender(ui)));
};

const dispatchEvt = (node: Node, type: string, data: any) => {
  const event = new Event(type, { bubbles: true });
  Object.assign(event, data);
  fireEvent.drop(node, event);
};

const mockData = (files: any[]) => ({
  dataTransfer: {
    files,
    items: files.map((file) => ({
      kind: 'file',
      type: file.type,
      getAsFile: () => file,
    })),
    types: ['Files'],
  },
});

describe('Test ImageUploadComponent', () => {
  afterEach(() => {
    mocked(readFileAsDataURL).mockClear();
  });

  it('Should test the file drop', async () => {
    mocked(readFileAsDataURL).mockResolvedValueOnce('base64url');

    const file = new File([
      'loremipsum',
    ], 'image.png', { type: 'image/png' });
    const data = mockData([file]);

    const ui = (
      <ImageUploadComponent setFaceSwapResult={jest.fn} />
    );
    const { rerender } = render(ui);
    const dropzone = screen.getByTestId('dropzone');

    dispatchEvt(dropzone, 'dragenter', data);
    await flushPromises(rerender, ui);

    await waitFor(() => {
      const uploadIcon = screen.queryByTestId('upload-icon');

      expect(uploadIcon).not.toBeInTheDocument();
    });
  });
});
