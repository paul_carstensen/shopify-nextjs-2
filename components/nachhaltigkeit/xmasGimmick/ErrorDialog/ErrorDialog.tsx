import React from 'react';
import Image from 'next/image';
import styled from 'styled-components';
import iceCreamCone from '../../../../assets/images/xmas-gimmick/ice-cream-cone.svg';

const ErrorWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100%;
  background-color: white;
  opacity: 0.8;
  cursor: pointer;
`;

const Text = styled.p`
  font-family: Poppins;
  font-size: 16px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  margin: 0;
`;

export const ErrorDialog = (): React.ReactElement => (
  <ErrorWrapper>
    <Image
      src={iceCreamCone}
      alt="Heruntergefallene Eiswaffel"
      height={70}
      width={70}
    />
    <Text>Etwas ist schief gelaufen...</Text>
    <Text>Lass es uns erneut versuchen!</Text>
  </ErrorWrapper>
);
