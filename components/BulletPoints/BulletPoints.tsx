import React from 'react';
import styled from 'styled-components';

const BulletPointsInner = styled.div`
  ul {
    display: flex;
    list-style-type: none;
    flex-wrap: wrap;
    font-size: 16px;
    font-weight: 500;
    line-height: 1.56;
    margin: auto;
    margin-bottom: 40px;
    width: 100%;
    li {
      width: 50%;
      display: flex;
      padding-bottom: 24px;
      &::before {
        content: '';
        background-image: url('https://cdn.shopify.com/s/files/1/0268/4819/8771/files/Bulletpoint_3x_b2f38760-dca0-4430-af81-a6cfa3f3f437_480x480.png?v=1627911365');
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
        width: 24px;
        height: 24px;
        display: block;
        margin-right: 20px;
      }
    }
    @media screen and (max-width: 768px) {
      flex-direction: column;
      li {
        width: 100%;
      }
    }
  }
`;

export interface BulletPointProps {
  children: React.ReactNode;
}

export const BulletPoints: React.FC<BulletPointProps> = ({ children }: BulletPointProps) => (
  <BulletPointsInner>
    {children}
  </BulletPointsInner>
);
