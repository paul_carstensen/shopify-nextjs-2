import React, { useState } from 'react';
import styled from 'styled-components';
import Image from 'next/image';
import { Button, Slider } from '@u2dv/marketplace_ui_kit/dist/components';
import IncomeImage from '../../../../assets/images/lifestyles/income.webp';
import GroupImage from '../../../../assets/images/lifestyles/group.webp';
import { Lifestyle, KeepMeUpdatedBody } from '../../../../types/risk/lifestyle';

type TabBodyProps = {
  lifestyle: Lifestyle | undefined
}

const TabBodyWrapper = styled.div`
  margin: 12px 0;
`;

const CoreGroupWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
`;

const CoreGroup = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;

  img {
    height: 40px !important;
    width: 40px !important;
  }
`;

const CoreGroupAmountWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  margin-right: 12px;
  font-weight: bold;
`;

const CoreGroupAmount = styled.span`
  font-size: 24px;
  line-height: normal;
  margin-right: 8px;
`;

const CoreGroupDataWrap = styled.span`
  display: flex;
  flex-direction: column;
  margin-left: 12px;
`;

const CoreGroupAgeText = styled.span`
  font-size: 12px;
  font-weight: 600;
  font-family: QuickSand;
`;

const SliderItemTitle = styled.span`
  font-size: 14px;
  font-weight: bold;
`;

const SliderItemDescription = styled.span`
  font-size: 12px;
  font-family: Quicksand;
`;

const Card = styled.div`
  display: flex;
  flex-direction: column;
  border-radius: 8px;
  padding: 38px 20px;
  box-shadow: 5px 10px 30px 0 rgba(178, 178, 178, 0.25);
  margin: 20px;
`;

const ExplanantionWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0px 25px;
  margin-bottom: 18px;
  padding-top: 25px;
`;

const ExplanationTitle = styled.span`
  font-size: 14px;
  font-weight: bold;
  text-align: left;
`;

const ExplanationDescription = styled.span`
  font-size: 12px;
  font-family: Quicksand;
  text-align: left;
`;

const ShowMore = styled.span`
  font-size: 12px;
  font-weight: bold;
  cursor: pointer;
  font-family: Poppins;
`;

export const TabBody: React.FC<TabBodyProps> = ({ lifestyle }) => {
  const [showMore, setShowMore] = useState(false);
  if (!lifestyle) {
    return null;
  }

  const onButtonClick = () => {
    const selectedLifestyleKey = lifestyle.lifestyleKey;
    const riskData = localStorage.getItem('riskData');
    const riskDataJson: KeepMeUpdatedBody = JSON.parse(riskData || '');
    const updatedRiskDataJson = {
      ...riskDataJson,
      selectedLifestyleKey,
    };
    localStorage.setItem('riskData', JSON.stringify(updatedRiskDataJson));
  };

  return (
    <TabBodyWrapper>
      <CoreGroupWrapper>
        <CoreGroup>
          <Image
            src={GroupImage}
            alt="group-icon"
          />
          <CoreGroupDataWrap>
            <CoreGroupAmountWrapper>
              <CoreGroupAmount>{`${lifestyle.kpiCoregroupAmount}`.replace('.', ',')}</CoreGroupAmount>
              <span>Mio.</span>
            </CoreGroupAmountWrapper>
            <CoreGroupAgeText>in DE ({lifestyle.kpiCoregroupAge} J. a.)</CoreGroupAgeText>
          </CoreGroupDataWrap>
        </CoreGroup>
        <CoreGroup>
          <Image
            src={IncomeImage}
            alt="income-icon"
          />
          <CoreGroupDataWrap>
            <CoreGroupAmountWrapper>
              <CoreGroupAmount>{`${lifestyle.kpiIncome}`}</CoreGroupAmount>
              <span>€</span>
            </CoreGroupAmountWrapper>
            <CoreGroupAgeText>Ø Einkommen<br /></CoreGroupAgeText>
          </CoreGroupDataWrap>
        </CoreGroup>
      </CoreGroupWrapper>
      <Slider
        visibleSlidesBreakpoints={[
          { breakpoint: '(max-width: 560px)', value: 1 },
          { breakpoint: '(min-width: 561px)', value: 1 },
        ]}
        enableArrows={false}
        enableMouseWheel
      >
        <Card>
          <SliderItemTitle>Grundüberzeugung</SliderItemTitle>
          <SliderItemDescription>{lifestyle.statementBeliefs}</SliderItemDescription>
        </Card>
        <Card>
          <SliderItemTitle>Grundüberzeugung</SliderItemTitle>
          <SliderItemDescription>{lifestyle.statementMotto}</SliderItemDescription>
        </Card>
        <Card>
          <SliderItemTitle>Grundüberzeugung</SliderItemTitle>
          <SliderItemDescription>{lifestyle.statementLifegoal}</SliderItemDescription>
        </Card>
      </Slider>
      <ExplanantionWrapper>
        <ExplanationTitle>Lifestyle Explanation:</ExplanationTitle>
        <ExplanationDescription>{showMore ? lifestyle.description : lifestyle.shortDescription} <ShowMore data-tracking-id="turbulanz-picklifestyle-more" onClick={() => setShowMore(!showMore)}>{showMore ? 'Weniger' : 'Mehr'}</ShowMore></ExplanationDescription>
      </ExplanantionWrapper>
      <Button
        trackingId={`turbulanz-picklifestyle-confirm-${lifestyle.lifestyleKey}`}
        appearance="primary"
        onClick={onButtonClick}
      >Yes, it is me!
      </Button>
    </TabBodyWrapper>
  );
};
