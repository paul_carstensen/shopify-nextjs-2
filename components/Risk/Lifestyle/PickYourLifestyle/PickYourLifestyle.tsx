import React, { useState } from 'react';
import styled from 'styled-components';

import { Tabs } from '../Tabs';
import { TabBody } from '../TabBody';
import { Lifestyle } from '../../../../types/risk/lifestyle';

type PickYourLifestyleProps = {
  pageTitle: string;
  pageDescription: string;
  lifestyles: Lifestyle[];
}

const PickYourLifestyleContainer = styled.div`
  max-width: 400px;
  width: 100%;
  margin: 0 auto;
  text-align: center;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  font-family: Poppins;
`;

const Title = styled.div`
  font-size: 20px;
  line-height: 24px;
  font-weight: 600;
  text-align: center;
  padding-bottom: 15px;
  font-weight: bold;
`;

const SubTitle = styled.div`
  text-align: center;
  font-size: 14px;
`;

export const PickYourLifestyle: React.FC<PickYourLifestyleProps> = ({
  pageTitle,
  pageDescription,
  lifestyles,
}) => {
  const [selectedLifestyle, setSelectedLifestyle] = useState(0);

  return (
    <PickYourLifestyleContainer>
      <Title>{pageTitle}</Title>
      <SubTitle>{pageDescription}</SubTitle>
      <Tabs lifestyles={lifestyles} selectedTabKey={selectedLifestyle} onChange={(selectedIndex) => setSelectedLifestyle(selectedIndex)} />
      <TabBody lifestyle={lifestyles[selectedLifestyle]} />
    </PickYourLifestyleContainer>
  );
};
