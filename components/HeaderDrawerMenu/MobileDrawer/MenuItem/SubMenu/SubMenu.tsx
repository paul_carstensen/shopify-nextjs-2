import React from 'react';
import { NavigationItem } from '../../../../../types';
import { SubMenuItem } from './SubMenuItem';

type SubMenuProps = {
  links: NavigationItem[];
  ariaLabelId: string;
  controlId: string;
}

export const SubMenu: React.FC<SubMenuProps> = ({ links, ariaLabelId, controlId }) => (
  <div
    id={controlId}
    className="mobile-nav__sublist collapsible-content collapsible-content--all"
    aria-labelledby={ariaLabelId}
  >
    <div className="collapsible-content__inner">
      <ul className="mobile-nav__sublist">
        {links.map((link, i) => <SubMenuItem link={link} controlId={`${controlId}-${i + 1}`} key={`${controlId}-${link.title}`} />)}
      </ul>
    </div>
  </div>
);
