import React from 'react';
import { NavigationItem } from '../../../../types';
import { SubMenu } from './SubMenu';
import { NavToggleButton } from './NavToggleButton';

type MenuItemProps = {
  link: NavigationItem;
  delay: number;
}

export const MenuItem: React.FC<MenuItemProps> = ({ link, delay }) => {
  const dashedLink = link.title.toLowerCase().replace(/( |[^a-züäö])/g, '-');
  const ariaLabelId = `Label-${dashedLink}`;
  const buttonControlId = `LinkList-${dashedLink}`;
  const hasChilds = link.items.length !== 0;
  return (
    <li className={`mobile-nav__item appear-animation appear-delay-${delay}`}>
      <div className={hasChilds ? 'mobile-nav__has-sublist' : ''}>
        <a href={link.url} className="mobile-nav__link mobile-nav__link--top-level" id={ariaLabelId}>
          {link.title}
        </a>
        {hasChilds && <NavToggleButton buttonControlId={buttonControlId} className="mobile-nav__toggle" buttonClassName="collapsible-trigger collapsible--auto-height" />}
      </div>

      {hasChilds && <SubMenu links={link.items} ariaLabelId={ariaLabelId} controlId={buttonControlId} />}
    </li>
  );
};
