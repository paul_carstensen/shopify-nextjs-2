import config from '../../../../config';
import { CrimeInfo } from '../../../../types/sicherheit/wie-sicher-ist-dein-wohnort';

const NOT_FOUND_MESSAGE = 'Die eingegebene Postleitzahl konnte nicht gefunden werden';

export const getCrimecheckInfo = async (postalCode: string): Promise<CrimeInfo> => {
  const response = await fetch(`${config().crimeCheck.apiUrl}/crime-info`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ postal_code: postalCode }),
  });

  if (response.status === 404) {
    throw new Error(NOT_FOUND_MESSAGE);
  }

  if (response.ok) {
    const { data } = await response.json();
    if (data) {
      return data as CrimeInfo;
    }
  }

  throw new Error('Ein Fehler ist aufgetreten');
};
