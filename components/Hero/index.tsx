import React from 'react';
import styled, { css } from 'styled-components';
import { AnchorHeaderHeroOffset } from '../common/AnchorHeaderHeroOffset';
import { PageWidthContainer } from '../common/PageWidthContainer';
import styles from './style.module.css';
import { ButtonLinkDark } from '../common/Buttons';

export interface HeroProps {
  title: string;
  subtitle: string;
  backgroundImages: string[];
  resolutions?: number[];
  ctaText?: string;
  ctaUrl?: string;
  contentAlign?: 'left' | 'center' | 'right';
}

type StyledHeroProps = {
  backgroundImages: string[]
  resolutions?: number[]
}

const StyledHero = styled.div<StyledHeroProps>`
  background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.5)), 
                    url(${({ backgroundImages }) => backgroundImages.length > 0 && backgroundImages[0]});

  ${({ backgroundImages, resolutions = [] }) => {
    let previousResolution = 1;
    return resolutions && resolutions.map((res, index) => {
      const cssBgImage = css`
        @media (min-width: ${previousResolution}px) and (max-width: ${res}px) {
          background-image: linear-gradient(to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.5)), 
                            url(${backgroundImages[index]});
        }`;
      previousResolution = res;
      return cssBgImage;
    });
  }}
`;

const HeroH1 = styled.h1`
  margin: 0 0 7.5px;
  font-family: "Quicksand", sans-serif;
  font-weight: 400;
  letter-spacing: 0em;
  line-height: 1;
  text-transform: uppercase;
  font-size: 2em;
  @media only screen and (min-width: 769px) {
    margin: 0 0 15px;
  }
`;

const HeroH2 = styled.h2`
  margin: 0 0 7.5px;
  font-family: "Quicksand", sans-serif;
  font-weight: 400;
  letter-spacing: 0em;
  line-height: 1;
  text-transform: uppercase;
  font-size: 1.72em;
  @media only screen and (min-width: 769px) {
    margin: 0 0 15px;
  }
`;

const Hero: React.FC<HeroProps> = (props: HeroProps) => {
  const {
    title, backgroundImages, subtitle, ctaText, ctaUrl, contentAlign, resolutions,
  } = props;

  const renderBtn = () => (
    <ButtonLinkDark
      href={ctaUrl}
      target="_blank"
      rel="noopener noreferrer"
      role="button"
    >
      {ctaText}
    </ButtonLinkDark>
  );

  return (
    <AnchorHeaderHeroOffset data-testid="hero">
      <PageWidthContainer>
        <div className={styles['hero-inner-container']}>
          <StyledHero
            className={`${styles.hero} hero`}
            backgroundImages={backgroundImages}
            resolutions={resolutions}
          >
            <div className={`${styles['hero-content']} ${styles[`hero-content-${contentAlign || 'left'}`]} hero-content`}>
              <HeroH1 className={styles.title}>{title}</HeroH1>
              <HeroH2 className={styles.subtitle}>{subtitle}</HeroH2>
              {ctaText && ctaUrl
                ? renderBtn()
                : null}
            </div>
          </StyledHero>
        </div>
      </PageWidthContainer>
    </AnchorHeaderHeroOffset>
  );
};

export default Hero;
