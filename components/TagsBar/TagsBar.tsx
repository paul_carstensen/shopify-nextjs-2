import React from 'react';
import styled from 'styled-components';
import { PageWidthContainer } from '../common/PageWidthContainer';
import { HeaderAnchorOffset } from '../common/HeaderAnchorOffset';

interface TagsBarItemProps {
  tag: string;
}

const TagItem = styled.div`
  background-color: #e6e6e6;
  margin: 0px 9px;
  padding: 4px 10px 4px 11px;
  color: #1d3345;
  font-size: 14px;
  line-height: 1.71;
  font-weight: 500;
  @media screen and (max-width: 768px) {
    margin: 0px 4px;
    font-size: 13px;
  }
`;

const TagsContainer = styled.div`
  display: flex;
  justify-content: center;
  margin: auto;
  margin-bottom: -20px;
`;

const TagsBarItem: React.FC<TagsBarItemProps> = ({ tag }) => (
  <TagItem>
    {tag}
  </TagItem>
);

export type TagsBarProps = {
  tags: string[];
  id?: string;
}

export const TagsBar: React.FC<TagsBarProps> = ({ tags, id }) => (
  <HeaderAnchorOffset id={id ?? 'tag_bar'}>
    <PageWidthContainer>
      <TagsContainer>
        {tags.map((tag) => <TagsBarItem key={tag} tag={tag} />)}
      </TagsContainer>
    </PageWidthContainer>
  </HeaderAnchorOffset>
);
