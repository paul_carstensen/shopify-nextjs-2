import React from 'react';
import { BlockContent as Props } from 'types/partnerPages';
import styles from './style.module.css';
import commonStyles from '../common.module.css';

interface Author {
  name: string;
  role: string;
  image: string;
}

interface Content {
  author: Author;
  description: string
}

export interface BlockProps {
  title: string;
  content: Content[];
  identifier: string;
}

const Block: React.FC<Props> = ({ data }: Props) => {
  const { title, content, identifier } = data as BlockProps;

  return (
    <div className={commonStyles['anchor-header-offset']} id={identifier}>
      <div className={`${commonStyles.container} ${styles.testimonial}`} id={identifier}>
        <div className={commonStyles['page-width']}>
          <div>
            <h2 className={commonStyles.light}>{title}</h2>
          </div>
          <div className={styles['testimonial-flex']}>
            {content.map((t, i) => (
              <div key={`content-${i.toString()}`} className={styles.item}>
                <img
                  src={t.author.image}
                  className={styles.image}
                  alt={t.description}
                />
                <div className={styles['description-content']}>
                  <p className={styles['testimonial-description']}>
                    {t.description}
                  </p>
                  <span className={styles['testimonial-author']}>{t.author.name}</span><br />
                  <span className={styles['testimonial-author-role']}>{t.author.role}</span>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Block;
