import React from 'react';
import { BlockContent as Props } from 'types/partnerPages';
import isInsideLinkAnchor from '../../../../helpers/partner_pages/isInsideLinkAnchor';
import styles from './style.module.css';
import commonStyles from '../common.module.css';
import emptyBigImage from '../../../../assets/images/partner-pages/empty-big.png';

export interface BlockProps {
  title: string;
  buttonText: string;
  buttonUrl: string;
  backgroundImage: string;
  subtitle: string;
  identifier: string;
}

const Block: React.FC<Props> = ({ data }: Props) => {
  const {
    title, buttonText, buttonUrl, backgroundImage, subtitle, identifier,
  } = data as BlockProps;

  const renderBtn = () => {
    if (isInsideLinkAnchor(buttonUrl)) {
      return <a href={buttonUrl} className={`${commonStyles.button} ${commonStyles.light} ${styles['hero-cta']} ${styles['ga-partner-page-hero-cta']}`} role="button">{buttonText}</a>;
    }

    return <a href={buttonUrl} target="_blank" rel="noopener noreferrer" className={`${commonStyles.button} ${commonStyles.light} ${styles['hero-cta']} ${styles['ga-partner-page-hero-cta']}`} role="button">{buttonText}</a>;
  };

  return (
    <div className={commonStyles['anchor-header-offset']} id={identifier}>
      <div className={styles['hero-page-width']}>
        <div className={styles['hero-inner-container']}>
          <div
            className={styles.hero}
            style={{
              backgroundImage: `linear-gradient(to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.5)), url(${backgroundImage || emptyBigImage.src})`,
            }}
          >
            <div className={styles['hero-content']}>
              <h1 className={styles.title}>{title}</h1>
              <h2 className={styles.subtitle}>{subtitle}</h2>
              {buttonText && buttonUrl
                ? renderBtn()
                : null}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Block;
