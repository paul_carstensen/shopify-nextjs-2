import React from 'react';
import styled from 'styled-components';
import { Button, SlideProps } from '@u2dv/marketplace_ui_kit/dist/components';
import { ButtonBar } from '../ButtonBar';

const SlideIntroContainer = styled.div<{ $padding: number }>`
  padding: 0 ${({ $padding }) => $padding}px;
`;

type SlideIntroProps = SlideProps & {
  padding: number;
  text: string;
  startButtonText?: string;
  startButtonTrackingId?: string;
  onSkipClick: () => void;
  skipButtonText?: string;
  skipButtonTrackingId?: string;
}

export const SlideIntro: React.FC<SlideIntroProps> = ({
  nextSlide, // injected by Slider component
  padding,
  text,
  startButtonText = 'Los geht\'s',
  startButtonTrackingId,
  onSkipClick,
  skipButtonTrackingId,
  skipButtonText = 'Überspringen',
}) => (
  <SlideIntroContainer $padding={padding}>
    <p>{text}</p>

    <ButtonBar>
      <Button trackingId={startButtonTrackingId} onClick={nextSlide}>{startButtonText}</Button>
      <Button appearance="secondary" trackingId={skipButtonTrackingId} onClick={onSkipClick}>{skipButtonText}</Button>
    </ButtonBar>
  </SlideIntroContainer>
);
