import React from 'react';
import { NavigationItem } from '../../../../types';
import { MegaMenuItem } from './MegaMenuItem';

type SubMenuMegaMenuProps = {
  links: NavigationItem[];
};

export const SubMenuMegaMenu: React.FC<SubMenuMegaMenuProps> = ({ links }) => (
  <div className="site-nav__dropdown megamenu text-left">
    <div className="page-width">
      <div className="grid grid--center">
        {links.map((link, i) => (<MegaMenuItem link={link} delay={i + 1} key={link.title} />))}
      </div>
    </div>
  </div>
);
