/* eslint-disable jsx-a11y/role-supports-aria-props */
import React from 'react';
import { NavigationItem } from '../../../types';
import { SubMenuDropDown } from './SubMenuDropDown';
import { SubMenuMegaMenu } from './SubMenuMegaMenu';

type MenuItemProps = {
  link: NavigationItem;
};

export const MenuItem: React.FC<MenuItemProps> = ({ link }) => {
  const hasDropdown = link.items.length !== 0;
  const isMegaMenu = link.items.some((subLink) => subLink.items.length !== 0);
  const hasDropdownClass = hasDropdown ? ' site-nav--has-dropdown' : '';
  const isMegaMenuClass = isMegaMenu ? ' site-nav--is-megamenu' : '';
  const hasDropdownLinkClass = hasDropdown ? ' site-nav__link--has-dropdown' : '';

  return (
    <li
      className={`nav-header-category site-nav__item site-nav__expanded-item${hasDropdownClass}${isMegaMenuClass}`}
      aria-haspopup={hasDropdown}
    >
      <a href={link.url} className={`main-category site-nav__link site-nav__link--underline${hasDropdownLinkClass}`}>
        {link.title}
      </a>
      {isMegaMenu && <SubMenuMegaMenu links={link.items} />}
      {!isMegaMenu && hasDropdown && <SubMenuDropDown links={link.items} />}
    </li>
  );
};
