import React from 'react';
import { NavigationItem } from '../../types';
import { MenuItem } from './MenuItem';

type HeaderMenuProps = {
  links: NavigationItem[];
};

export const HeaderMenu: React.FC<HeaderMenuProps> = ({ links }) => (
  <ul className="site-nav site-navigation small--hide">
    {
      links.map((link) => <MenuItem link={link} key={link.url} />)
    }
  </ul>
);
