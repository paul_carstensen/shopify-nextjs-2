import React from 'react';
import styled from 'styled-components';
import Header from './Header';
import Footer from './Footer';
import { NavigationItem, Navigation } from '../types';

const MainContentContainer = styled.main`
  margin-top: 30px;
  display: block;
  min-height: 300px;

  @media only screen and (min-width: 769px) {
      min-height: 700px;
  }
`;

type LayoutProps = {
  footerNavigationItems: Navigation[];
  mainNavigationItems: NavigationItem[];
  sideNavigationItems: NavigationItem[];
};

const Layout: React.FC<LayoutProps> = ({ footerNavigationItems, mainNavigationItems, sideNavigationItems, children }) => (
  <>
    <div id="PageContainer" className="page-container">
      <Header mainNavigationItems={mainNavigationItems} sideNavigationItems={sideNavigationItems} />
      <MainContentContainer id="MainContent">
        {children}
      </MainContentContainer>
      <Footer footerNavigationItems={footerNavigationItems} />
    </div>
  </>
);

export default Layout;
