import React, { ReactElement } from 'react';
import styles from './ProductHighlight.module.css';

type ProductHighlightProps = {
  collectionHandle: string;
  handle: string;
  price: number;
  originalPrice: number;
  imageUrl: string;
  title: string;
}

const getDiscountPercentage = (price: number, originalPrice: number) => ((originalPrice - price) * 100) / originalPrice;

const renderPrice = (price: number, originalPrice: number) => {
  const discountPercentage = getDiscountPercentage(price, originalPrice);
  if (discountPercentage > 5) {
    return (
      <div className={styles.price_container}>
        <span className="visually-hidden">Normaler Preis</span>
        <span className={styles.price_original}>€{originalPrice}</span>
        <span className="visually-hidden">Sonderpreis</span>
        €{price}
        <span className={styles.price_discount}>
          Spare {discountPercentage.toFixed(0)}%
        </span>
      </div>
    );
  }
  return (
    <div className={styles.price_container}>
      €{price}
    </div>
  );
};

const renderImage = (imageUrl: string, alt: string, price: number, originalPrice: number) => {
  const discountPercentage = getDiscountPercentage(price, originalPrice);
  let discountBadge: ReactElement | null = null;
  if (discountPercentage > 5) {
    discountBadge = <span className={styles.discount_badge}>Reduziert</span>;
  }
  return (
    <>
      {discountBadge}
      <div className={styles.image_container}>
        <img
          className={styles.image}
          src={imageUrl}
          alt={alt}
        />
      </div>
    </>
  );
};

const ProductHighlight: React.FC<ProductHighlightProps> = (
  { collectionHandle, handle, price, originalPrice, imageUrl, title },
) => (
  <div className={styles.product} data-tracking-id="ga-blog-product-highlight-click">
    <div>
      <a href={`/collections/${collectionHandle}/products/${handle}`}>
        {renderImage(imageUrl, title, price, originalPrice)}
        <div className={styles.product_title}>
          <div>
            {title}
          </div>
          {renderPrice(price, originalPrice)}
        </div>
      </a>
    </div>
  </div>
);

export default ProductHighlight;
