import axios from 'axios';
import { FaceSwapObject } from '../../../components/nachhaltigkeit/xmasGimmick/types';
import config from '../../../config';

export enum Genders {
  FEMALE = 'female',
  MALE = 'male',
}

export const faceSwap = async (
  { image, gender }: { image: string, gender?: Genders | null },
): Promise<FaceSwapObject> => (
  axios({
    method: 'POST',
    url: `${config().faceSwap.apiUrl}/faceswap`,
    data: {
      image: image.split('base64,')[1],
      gender,
    },
  }).then((r) => r.data as FaceSwapObject)
);
