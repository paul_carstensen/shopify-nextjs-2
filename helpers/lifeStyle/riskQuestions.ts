import imageRiskQuestion1 from '../../assets/images/risk/riskQuestion_1.jpg';
import imageRiskQuestion2 from '../../assets/images/risk/riskQuestion_2.jpg';
import imageRiskQuestion3 from '../../assets/images/risk/riskQuestion_3.jpg';
import imageRiskQuestion4 from '../../assets/images/risk/riskQuestion_4.jpg';
import imageRiskQuestion5 from '../../assets/images/risk/riskQuestion_5.jpg';
import imageRiskQuestion6 from '../../assets/images/risk/riskQuestion_6.jpg';
import imageRiskQuestion7 from '../../assets/images/risk/riskQuestion_7.jpg';
import imageRiskQuestion8 from '../../assets/images/risk/riskQuestion_8.jpg';
import imageRiskQuestion9 from '../../assets/images/risk/riskQuestion_9.jpg';
import imageRiskQuestion10 from '../../assets/images/risk/riskQuestion_10.jpg';

const RiskQuestions = [
  {
    questions: 'Ich bin ein Pionier des Wandels',
    bgImage: imageRiskQuestion1.src,
    answers: [
      {
        answer: 'Trifft voll und ganz zu',
        answerId: 6,
        lifeStyle: [
          'online_avatar', 'visionary', 'storyteller',
        ],
        dataTrackingId: 'turbulanz-question-answer-01_06',
      },
      {
        answer: 'Trifft zu',
        answerId: 5,
        lifeStyle: [
          'le_hipster', 'hip_and_hopper', 'world_pilot',
        ],
        dataTrackingId: 'turbulanz-question-answer-01_05',
      },
      {
        answer: 'Mal so mal so',
        answerId: 4,
        lifeStyle: [
          'solo_dj', 'multitasker', 'urban_guy',
        ],
        dataTrackingId: 'turbulanz-question-answer-01_04',
      },
      {
        answer: 'Kann ich nicht beurteilen',
        answerId: 3,
        lifeStyle: [
          'freedom_mind', 'luxury_lover', 'yogi_guru',
        ],
        dataTrackingId: 'turbulanz-question-answer-01_03',
      },
      {
        answer: 'Trifft nicht zu',
        answerId: 2,
        lifeStyle: [
          'mind_maker', 'social_politician', 'silver_athlete',
        ],
        dataTrackingId: 'turbulanz-question-answer-01_02',
      },
      {
        answer: 'Trifft gar nicht zu',
        answerId: 1,
        lifeStyle: [
          'gatekeeper', 'sun_herdsman', 'autopilot',
        ],
        dataTrackingId: 'turbulanz-question-answer-01_01',
      },
    ],
  },
  {
    questions: 'Ich stehe für Achtsamkeit und Werte der Entschleunigung',
    bgImage: imageRiskQuestion2.src,
    answers: [
      {
        answer: 'Trifft voll und ganz zu',
        answerId: 6,
        lifeStyle: [
          'yogi_guru', 'le_hipster', 'mind_maker',
        ],
        dataTrackingId: 'turbulanz-question-answer-02_06',
      },
      {
        answer: 'Trifft zu',
        answerId: 5,
        lifeStyle: [
          'sun_herdsman', 'social_politician', 'silver_athlete',
        ],
        dataTrackingId: 'turbulanz-question-answer-02_05',
      },
      {
        answer: 'Mal so mal so',
        answerId: 4,
        lifeStyle: [
          'autopilot', 'gatekeeper', 'storyteller',
        ],
        dataTrackingId: 'turbulanz-question-answer-02_04',
      },
      {
        answer: 'Kann ich nicht beurteilen',
        answerId: 3,
        lifeStyle: [
          'visionary', 'urban_guy', 'world_pilot',
        ],
        dataTrackingId: 'turbulanz-question-answer-02_03',
      },
      {
        answer: 'Trifft nicht zu',
        answerId: 2,
        lifeStyle: [
          'freedom_mind', 'online_avatar', 'luxury_lover',
        ],
        dataTrackingId: 'turbulanz-question-answer-02_02',
      },
      {
        answer: 'Trifft gar nicht zu',
        answerId: 1,
        lifeStyle: [
          'solo_dj', 'hip_and_hopper', 'multitasker',
        ],
        dataTrackingId: 'turbulanz-question-answer-02_01',
      },
    ],
  },
  {
    questions: 'Ich bin ein Trendsetter',
    bgImage: imageRiskQuestion3.src,
    answers: [
      {
        answer: 'Trifft voll und ganz zu',
        answerId: 6,
        lifeStyle: [
          'urban_guy', 'online_avatar', 'storyteller',
        ],
        dataTrackingId: 'turbulanz-question-answer-03_06',
      },
      {
        answer: 'Trifft zu',
        answerId: 5,
        lifeStyle: [
          'world_pilot', 'visionary', 'le_hipster',
        ],
        dataTrackingId: 'turbulanz-question-answer-03_05',
      },
      {
        answer: 'Mal so mal so',
        answerId: 4,
        lifeStyle: [
          'hip_and_hopper', 'luxury_lover', 'multitasker',
        ],
        dataTrackingId: 'turbulanz-question-answer-03_04',
      },
      {
        answer: 'Kann ich nicht beurteilen',
        answerId: 3,
        lifeStyle: [
          'solo_dj', 'social_politician', 'silver_athlete',
        ],
        dataTrackingId: 'turbulanz-question-answer-03_03',
      },
      {
        answer: 'Trifft nicht zu',
        answerId: 2,
        lifeStyle: [
          'yogi_guru', 'sun_herdsman', 'gatekeeper',
        ],
        dataTrackingId: 'turbulanz-question-answer-03_02',
      },
      {
        answer: 'Trifft gar nicht zu',
        answerId: 1,
        lifeStyle: [
          'autopilot', 'freedom_mind', 'mind_maker',
        ],
        dataTrackingId: 'turbulanz-question-answer-03_01',
      },
    ],
  },
  {
    questions: 'Ich stehe für Nachhaltigen Wachstum',
    bgImage: imageRiskQuestion4.src,
    answers: [
      {
        answer: 'Trifft voll und ganz zu',
        answerId: 6,
        lifeStyle: [
          'visionary', 'mind_maker', 'le_hipster',
        ],
        dataTrackingId: 'turbulanz-question-answer-04_06',
      },
      {
        answer: 'Trifft zu',
        answerId: 5,
        lifeStyle: [
          'freedom_mind', 'yogi_guru', 'social_politician',
        ],
        dataTrackingId: 'turbulanz-question-answer-04_05',
      },
      {
        answer: 'Mal so mal so',
        answerId: 4,
        lifeStyle: [
          'autopilot', 'sun_herdsman', 'gatekeeper',
        ],
        dataTrackingId: 'turbulanz-question-answer-04_04',
      },
      {
        answer: 'Kann ich nicht beurteilen',
        answerId: 3,
        lifeStyle: [
          'urban_guy', 'storyteller', 'silver_athlete',
        ],
        dataTrackingId: 'turbulanz-question-answer-04_03',
      },
      {
        answer: 'Trifft nicht zu',
        answerId: 2,
        lifeStyle: [
          'online_avatar', 'luxury_lover', 'world_pilot',
        ],
        dataTrackingId: 'turbulanz-question-answer-04_02',
      },
      {
        answer: 'Trifft gar nicht zu',
        answerId: 1,
        lifeStyle: [
          'solo_dj', 'hip_and_hopper', 'multitasker',
        ],
        dataTrackingId: 'turbulanz-question-answer-04_01',
      },
    ],
  },
  {
    questions: 'Ich interessiere mich für zukünftiger Wohnkonzepte ',
    bgImage: imageRiskQuestion5.src,
    answers: [
      {
        answer: 'Trifft voll und ganz zu',
        answerId: 6,
        lifeStyle: [
          'world_pilot', 'urban_guy', 'sun_herdsman',
        ],
        dataTrackingId: 'turbulanz-question-answer-05_06',
      },
      {
        answer: 'Trifft zu',
        answerId: 5,
        lifeStyle: [
          'yogi_guru', 'le_hipster', 'hip_and_hopper',
        ],
        dataTrackingId: 'turbulanz-question-answer-05_05',
      },
      {
        answer: 'Mal so mal so',
        answerId: 4,
        lifeStyle: [
          'freedom_mind', 'luxury_lover', 'visionary',
        ],
        dataTrackingId: 'turbulanz-question-answer-05_04',
      },
      {
        answer: 'Kann ich nicht beurteilen',
        answerId: 3,
        lifeStyle: [
          'online_avatar', 'multitasker', 'mind_maker',
        ],
        dataTrackingId: 'turbulanz-question-answer-05_03',
      },
      {
        answer: 'Trifft nicht zu',
        answerId: 2,
        lifeStyle: [
          'solo_dj', 'gatekeeper', 'autopilot',
        ],
        dataTrackingId: 'turbulanz-question-answer-05_02',
      },
      {
        answer: 'Trifft gar nicht zu',
        answerId: 1,
        lifeStyle: [
          'silver_athlete', 'social_politician', 'storyteller',
        ],
        dataTrackingId: 'turbulanz-question-answer-05_01',
      },
    ],
  },
  {
    questions: 'Ich begebe mich auf Kulinalische Abenteuer',
    bgImage: imageRiskQuestion6.src,
    answers: [
      {
        answer: 'Trifft voll und ganz zu',
        answerId: 6,
        lifeStyle: [
          'urban_guy', 'multitasker', 'social_politician',
        ],
        dataTrackingId: 'turbulanz-question-answer-06_06',
      },
      {
        answer: 'Trifft zu',
        answerId: 5,
        lifeStyle: [
          'world_pilot', 'le_hipster', 'silver_athlete',
        ],
        dataTrackingId: 'turbulanz-question-answer-06_05',
      },
      {
        answer: 'Mal so mal so',
        answerId: 4,
        lifeStyle: [
          'luxury_lover', 'sun_herdsman', 'mind_maker',
        ],
        dataTrackingId: 'turbulanz-question-answer-06_04',
      },
      {
        answer: 'Kann ich nicht beurteilen',
        answerId: 3,
        lifeStyle: [
          'hip_and_hopper', 'yogi_guru', 'autopilot',
        ],
        dataTrackingId: 'turbulanz-question-answer-06_03',
      },
      {
        answer: 'Trifft nicht zu',
        answerId: 2,
        lifeStyle: [
          'freedom_mind', 'visionary', 'gatekeeper',
        ],
        dataTrackingId: 'turbulanz-question-answer-06_02',
      },
      {
        answer: 'Trifft gar nicht zu',
        answerId: 1,
        lifeStyle: [
          'online_avatar', 'storyteller', 'solo_dj',
        ],
        dataTrackingId: 'turbulanz-question-answer-06_01',
      },
    ],
  },
  {
    questions: 'Ich bin Digital affin',
    bgImage: imageRiskQuestion7.src,
    answers: [
      {
        answer: 'Trifft voll und ganz zu',
        answerId: 6,
        lifeStyle: [
          'online_avatar', 'world_pilot', 'multitasker',
        ],
        dataTrackingId: 'turbulanz-question-answer-07_06',
      },
      {
        answer: 'Trifft zu',
        answerId: 5,
        lifeStyle: [
          'luxury_lover', 'hip_and_hopper', 'visionary',
        ],
        dataTrackingId: 'turbulanz-question-answer-07_05',
      },
      {
        answer: 'Mal so mal so',
        answerId: 4,
        lifeStyle: [
          'freedom_mind', 'yogi_guru', 'autopilot',
        ],
        dataTrackingId: 'turbulanz-question-answer-07_04',
      },
      {
        answer: 'Kann ich nicht beurteilen',
        answerId: 3,
        lifeStyle: [
          'urban_guy', 'mind_maker', 'le_hipster',
        ],
        dataTrackingId: 'turbulanz-question-answer-07_03',
      },
      {
        answer: 'Trifft nicht zu',
        answerId: 2,
        lifeStyle: [
          'storyteller', 'silver_athlete', 'solo_dj',
        ],
        dataTrackingId: 'turbulanz-question-answer-07_02',
      },
      {
        answer: 'Trifft gar nicht zu',
        answerId: 1,
        lifeStyle: [
          'gatekeeper', 'sun_herdsman', 'social_politician',
        ],
        dataTrackingId: 'turbulanz-question-answer-07_01',
      },
    ],
  },
  {
    questions: 'Mein Gesundheitsbewusstsein prägt meinen Alltag',
    bgImage: imageRiskQuestion8.src,
    answers: [
      {
        answer: 'Trifft voll und ganz zu',
        answerId: 6,
        lifeStyle: [
          'yogi_guru', 'silver_athlete', 'multitasker',
        ],
        dataTrackingId: 'turbulanz-question-answer-08_06',
      },
      {
        answer: 'Trifft zu',
        answerId: 5,
        lifeStyle: [
          'freedom_mind', 'sun_herdsman', 'mind_maker',
        ],
        dataTrackingId: 'turbulanz-question-answer-08_05',
      },
      {
        answer: 'Mal so mal so',
        answerId: 4,
        lifeStyle: [
          'autopilot', 'social_politician', 'le_hipster',
        ],
        dataTrackingId: 'turbulanz-question-answer-08_04',
      },
      {
        answer: 'Kann ich nicht beurteilen',
        answerId: 3,
        lifeStyle: [
          'online_avatar', 'hip_and_hopper', 'world_pilot',
        ],
        dataTrackingId: 'turbulanz-question-answer-08_03',
      },
      {
        answer: 'Trifft nicht zu',
        answerId: 2,
        lifeStyle: [
          'luxury_lover', 'urban_guy', 'gatekeeper',
        ],
        dataTrackingId: 'turbulanz-question-answer-08_02',
      },
      {
        answer: 'Trifft gar nicht zu',
        answerId: 1,
        lifeStyle: [
          'solo_dj', 'visionary', 'storyteller',
        ],
        dataTrackingId: 'turbulanz-question-answer-08_01',
      },
    ],
  },
  {
    questions: 'Familie geht über alles',
    bgImage: imageRiskQuestion9.src,
    answers: [
      {
        answer: 'Trifft voll und ganz zu',
        answerId: 6,
        lifeStyle: [
          'freedom_mind', 'storyteller', 'sun_herdsman',
        ],
        dataTrackingId: 'turbulanz-question-answer-09_06',
      },
      {
        answer: 'Trifft zu',
        answerId: 5,
        lifeStyle: [
          'multitasker', 'le_hipster', 'gatekeeper',
        ],
        dataTrackingId: 'turbulanz-question-answer-09_05',
      },
      {
        answer: 'Mal so mal so',
        answerId: 4,
        lifeStyle: [
          'yogi_guru', 'autopilot', 'silver_athlete',
        ],
        dataTrackingId: 'turbulanz-question-answer-09_04',
      },
      {
        answer: 'Kann ich nicht beurteilen',
        answerId: 3,
        lifeStyle: [
          'hip_and_hopper', 'visionary', 'social_politician',
        ],
        dataTrackingId: 'turbulanz-question-answer-09_03',
      },
      {
        answer: 'Trifft nicht zu',
        answerId: 2,
        lifeStyle: [
          'online_avatar', 'world_pilot', 'urban_guy',
        ],
        dataTrackingId: 'turbulanz-question-answer-09_02',
      },
      {
        answer: 'Trifft gar nicht zu',
        answerId: 1,
        lifeStyle: [
          'solo_dj', 'luxury_lover', 'mind_maker',
        ],
        dataTrackingId: 'turbulanz-question-answer-09_01',
      },
    ],
  },
  {
    questions: 'Alter',
    bgImage: imageRiskQuestion10.src,
    answers: [
      {
        answer: 'Trifft voll und ganz zu',
        answerId: 6,
        lifeStyle: [
          'online_avatar', 'hip_and_hopper', 'luxury_lover',
        ],
        dataTrackingId: 'turbulanz-question-answer-10_06',
      },
      {
        answer: 'Trifft zu',
        answerId: 5,
        lifeStyle: [
          'solo_dj', 'yogi_guru', 'visionary',
        ],
        dataTrackingId: 'turbulanz-question-answer-10_05',
      },
      {
        answer: 'Mal so mal so',
        answerId: 4,
        lifeStyle: [
          'world_pilot', 'autopilot', 'multitasker',
        ],
        dataTrackingId: 'turbulanz-question-answer-10_04',
      },
      {
        answer: 'Kann ich nicht beurteilen',
        answerId: 3,
        lifeStyle: [
          'freedom_mind', 'sun_herdsman', 'urban_guy',
        ],
        dataTrackingId: 'turbulanz-question-answer-10_03',
      },
      {
        answer: 'Trifft nicht zu',
        answerId: 2,
        lifeStyle: [
          'mind_maker', 'social_politician', 'le_hipster',
        ],
        dataTrackingId: 'turbulanz-question-answer-10_02',
      },
      {
        answer: 'Trifft gar nicht zu',
        answerId: 1,
        lifeStyle: [
          'gatekeeper', 'storyteller', 'silver_athlete',
        ],
        dataTrackingId: 'turbulanz-question-answer-10_01',
      },
    ],
  },
];

export default RiskQuestions;
