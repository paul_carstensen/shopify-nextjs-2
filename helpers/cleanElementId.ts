export const cleanElementId = (id: string): string => (
  id.toLowerCase().replace(/[^a-z_-]/g, '_')
);
