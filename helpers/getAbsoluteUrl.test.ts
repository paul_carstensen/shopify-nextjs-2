import getAbsoluteUrl from './getAbsoluteUrl';

describe('getAbsoluteUrl', () => {
  const testRequest = {
    url: '/test',
    headers: {
      host: 'example.com',
    },
  } as any;

  it('should determine the correct absolute URL', () => {
    expect(getAbsoluteUrl(testRequest).toString()).toEqual('https://example.com/test');
  });

  it('should use the optional override path if given', () => {
    expect(getAbsoluteUrl(testRequest, '/path').toString()).toEqual('https://example.com/path');
  });

  it('should use an empty override path if given', () => {
    expect(getAbsoluteUrl(testRequest, '').toString()).toEqual('https://example.com/');
  });

  it('should throw an error if the request path is unknown', () => {
    const unknownUrlRequest = {
      ...testRequest,
      url: undefined,
    };

    expect(() => getAbsoluteUrl(unknownUrlRequest)).toThrow();
  });
});
