import Compressor from 'compressorjs';

const readFileAsDataURL = async (
  originalFile: File,
  compress?: number,
  maxHeight?: number,
  maxWidth?: number,
): Promise<string | null> => new Promise((resolve) => {
  if (!originalFile) {
    return;
  }

  const readFile = (file: File) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      resolve(reader.result as string);
    });

    reader.readAsDataURL(file);
  };

  if (!compress) {
    readFile(originalFile);
  } else {
    // eslint-disable-next-line no-new
    new Compressor(originalFile, {
      quality: compress,
      success: readFile,
      maxHeight,
      maxWidth,
    });
  }
});

export default readFileAsDataURL;
