import React, { useState, useContext, createContext, useCallback, ReactNode } from 'react';

interface IApiErrorsContext {
  setErrors: (path: string, value: string) => void
  errors: Record<string, string>
}

const ApiErrorsContext = createContext<IApiErrorsContext | Record<string, never>>({});

export const useApiErrors = () => useContext(ApiErrorsContext);

const ApiErrorsProvider = ({ children } : { children?: ReactNode }) => {
  const [state, setState] = useState<Record<string, string>>({});

  const setErrors = useCallback((path: string, value: string) => {
    setState({
      ...state,
      [path]: value,
    });
  }, [state]);

  return (
    <ApiErrorsContext.Provider value={{ errors: state, setErrors }}>
      {children}
    </ApiErrorsContext.Provider>
  );
};

export default ApiErrorsProvider;
