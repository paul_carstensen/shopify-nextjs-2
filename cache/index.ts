import NodeCache from 'node-cache';

export const keys = {
  NAVIGATION_MENU_ITEMS: 'navigationMenuItems',
  RADON_POLICY_TEXT: 'radonPolicyText',
  LIFESTYLE_POLICY_TEXT: 'lifestylePolicyText',
};

export default new NodeCache();
