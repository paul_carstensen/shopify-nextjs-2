export type NavigationItem = {
    title: string;
    url: string;
    items: NavigationItem[];
}

export type Navigation = {
    title: string;
    items: NavigationItem[];
}

export type PageUrl = string
