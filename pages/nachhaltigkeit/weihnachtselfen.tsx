import React, { ReactElement, useState } from 'react';
import { NextPage, GetServerSideProps, GetServerSidePropsContext } from 'next';
import Head from 'next/head';
import { HeaderAnchorOffset } from '../../components/common/HeaderAnchorOffset';
import { PageWidthContainer } from '../../components/common/PageWidthContainer';
import { XmasGimmickHero } from '../../components/nachhaltigkeit/xmasGimmick/XmasGimmickHero';
import { XmasFirstScreen } from '../../components/nachhaltigkeit/xmasGimmick/XmasFirstScreen';
import { XmasResultScreen } from '../../components/nachhaltigkeit/xmasGimmick/XmasResultScreen';
import getAbsoluteUrl from '../../helpers/getAbsoluteUrl';
import xmasGimmickImage from '../../assets/images/xmas-gimmick/xmas-gimmick.jpg';
import xmasGimmickImageMob from '../../assets/images/xmas-gimmick/xmas-gimmick-mob.jpg';
import { FaceSwapObject } from '../../components/nachhaltigkeit/xmasGimmick/types';

type XmasGimmickPageProps = {
  socialMediaImageUrl: string;
}

const XmasGimmickPage: NextPage<XmasGimmickPageProps> = ({ socialMediaImageUrl }) => {
  const [faceSwapResult, setFaceSwapResult] = useState<FaceSwapObject | null>(null);

  const renderPage = (): ReactElement => (
    <HeaderAnchorOffset id="xmas-gimmick">
      <PageWidthContainer>
        <XmasGimmickHero
          title="Welcher Elf bist du?"
          backgroundImage={xmasGimmickImage.src}
          backgroundImageMob={xmasGimmickImageMob.src}
        />
        {
          !faceSwapResult
            ? (
              <XmasFirstScreen
                setFaceSwapResult={setFaceSwapResult}
              />
            )
            : (
              <XmasResultScreen
                faceSwapResult={faceSwapResult}
                setFaceSwapResult={setFaceSwapResult}
              />
            )
        }
      </PageWidthContainer>
    </HeaderAnchorOffset>
  );

  const title = 'Deine personalisierte Weihnachtskarte';
  const description = 'Welcher Elf bist Du? Finde es heraus mit Deiner persönlichen Weihnachtself-Karte ✓ Einfach Foto hochladen ➤ uptodate.de';

  return (
    <>
      <Head>
        <title>{title}</title>
        <meta name="description" content={description} />

        <meta key="og:title" property="og:title" content={title} />
        <meta key="og:description" property="og:description" content={description} />
        <meta key="og:image" property="og:image" content={socialMediaImageUrl} />
      </Head>

      {renderPage()}
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async ({ req }: GetServerSidePropsContext) => ({
  props: {
    socialMediaImageUrl: getAbsoluteUrl(req, xmasGimmickImage.src).toString(),
  },
});

export default XmasGimmickPage;
