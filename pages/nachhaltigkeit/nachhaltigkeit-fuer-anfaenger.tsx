import React from 'react';
import { NextPage } from 'next';
import Head from 'next/head';
import { scrollToSlider } from '../../helpers/scrollToSlider';
import { SliderPage, SLIDER_PAGE_SCROLL_CONTAINER_ID } from '../../components/SliderPage';
import { SliderEcosia } from '../../components/nachhaltigkeit/sustainability/SliderEcosia';
import { SliderAvoidWaste } from '../../components/nachhaltigkeit/sustainability/SliderAvoidWaste';
import { SliderEarnest } from '../../components/nachhaltigkeit/sustainability/SliderEarnest';
import { SliderSmartricity } from '../../components/nachhaltigkeit/sustainability/SliderSmartricity';
import { SliderCheck24 } from '../../components/nachhaltigkeit/sustainability/SliderCheck24';

const title = 'Nachhaltiger leben - Umwelt bewahren';

const SustainabilityForBeginnersPage: NextPage = () => (
  <>
    <Head>
      <title>{title}</title>
      <meta name="description" content="" />

      <meta key="og:title" property="og:title" content={title} />
      <meta key="og:description" property="og:description" content="" />
      <meta key="og:image" property="og:image" content="" />
    </Head>

    <SliderPage
      backgroundColor="#e6e6e6"
      title="Nachhaltiger leben - Umwelt bewahren"
      subtitle="Werde in weniger als 10 Minuten nachhaltiger!"
      text="Nachhaltigkeit muss nicht immer kompliziert und teuer sein! Es gibt viele kleine Dinge, die Du sofort ändern kannst. Wie und was? Das zeigen wir Dir hier."
    >
      <SliderEcosia nextTipp={() => scrollToSlider('#tipp-slide-2', SLIDER_PAGE_SCROLL_CONTAINER_ID)} />
      <SliderAvoidWaste nextTipp={() => scrollToSlider('#tipp-slide-3', SLIDER_PAGE_SCROLL_CONTAINER_ID)} />
      <SliderSmartricity nextTipp={() => scrollToSlider('#tipp-slide-4', SLIDER_PAGE_SCROLL_CONTAINER_ID)} />
      <SliderCheck24 nextTipp={() => scrollToSlider('#tipp-slide-5', SLIDER_PAGE_SCROLL_CONTAINER_ID)} />
      <SliderEarnest />
    </SliderPage>
  </>
);

export default SustainabilityForBeginnersPage;
